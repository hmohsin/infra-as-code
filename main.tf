provider "aws" {
  region  = var.AWS_REGION
  profile = var.AWS_PROFILE
}

terraform {

  backend "s3" {
    bucket  = "terraform-jenkins-states-files"
    encrypt = "true"
    region  = "us-east-1"
    key     = "demo/terraform.tfstate"
    profile = "mh"
  }
}


module "main-vpc" {
  source     = "./modules/vpc"
  ENV        = var.ENV
  AWS_REGION = var.AWS_REGION
}

module "user_data" {
  source  = "./modules/user_data"
}

module "instances" {
  source         = "./modules/instances"
  KEY_NAME       = var.KEY_NAME
  ENV            = var.ENV
  VPC_ID         = module.main-vpc.vpc_id
  PUBLIC_SUBNETS = module.main-vpc.public_subnets
  USER_DATA      = module.user_data.cloudinit_config
}
