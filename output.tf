output "EC2_PUBLIC_IP" {
  value = module.instances.EC2_PUBLIC_IP
}

output "EC2_PUBLIC_DNS" {
  value = module.instances.EC2_PUBLIC_DNS
}

output "key_name" {
  value = module.instances.key_name
}
