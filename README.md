# EC2 Instance Infra-As-Code

## Install & configure AWSClI

### Install AWSCLI
1. Download & install for [Windows](https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi)
2. Download & install for [Ubunu](https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html)

#### Configure AWSCLI
1. First you need to generate aws access-key & secret-id. [More Info](https://docs.aws.amazon.com/general/latest/gr/managing-aws-access-keys.html)
2. Run below command on windows as well as linux.
```
aws configure --profile mh
```
## Usage

To run this example you need to execute:

```bash
terraform init
terraform plan -out terraform.plan
terraform apply terraform.plan
```

## Variables

- `AWS_REGION` - Name of the AWS Region
- `AWS_PROFILE` - Name of aws credentials
- `ENV` - Name of the Environment of application `stage` `prod` `testing` (default: `dev`)
- `KEY_NAME` - Name of EC2 Instance keyname.