data "template_file" "init-script" {
  template = file("${path.module}/scripts/init.cfg")
}

data "template_cloudinit_config" "cloudinit-example" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.init-script.rendered
  }
}

output "cloudinit_config" {
  value = "${data.template_cloudinit_config.cloudinit-example.rendered}"
}

